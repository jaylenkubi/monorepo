import { DataSource } from "typeorm";
import { Config, TemplateConfig } from "./config.interface";
import { PostgresConnectionOptions } from "typeorm/driver/postgres/PostgresConnectionOptions";


let PGDataSource: DataSource;
let configObj: Config;
let rawConfigObj: any;

export const transform = (directory: string): Config => {
	return {
		serviceName: `${process.env.DIVISION}-${process.env.SYSTEM}-api`,
		env: "dev",
		jwt: {
			secret: "secret",
			accessExpirationMinutes: 30,
			refreshExpirationDays: 30,
			resetPasswordExpirationMinutes: "30m"
		},
		typeOrm: {
			type: "postgres",
			host: "localhost",
			port: 5432,
			username: "postgres",
			password: "password",
			database: "postgres",
			synchronize: true,
			logging: true,
			entities: [`${directory}/../api/entity/*{.ts,.js}`],
			migrations: [`${directory}/../api/entity/migrations/*{.ts,.js}`],
			cache: false,
			extra: JSON.stringify({ socketPath: 'localhost' }),
			cli: { migrationsDir: `${directory}/../api/entity/migrations/` },
			migrationsRun: false
		}
	};
};

export const config = (): Config => {
	return configObj;
};

export const rawConfig = (): any => {
	return rawConfigObj;
};

export const pgDataSource = (): DataSource => {
	return PGDataSource;
};

export const fetchConfig = async (directory: string): Promise<void> => {
	rawConfigObj = process.env;
	configObj = transform(directory);
	PGDataSource = await new DataSource({
		...configObj.typeOrm,
		autoLoadEntities: true,
		poolSize: 50
	} as PostgresConnectionOptions).initialize();
}

export const baseInitializeConfig = async (jwtStrategy: any, directory: string):Promise<TemplateConfig>  => {
	await fetchConfig(directory);
	const currentConfig = config();
	return {
		config: {
			...currentConfig,
			serviceName: `${process.env.DIVISION}-${process.env.SYSTEM}-api`
		},
		jwtStrategy
	}
};

