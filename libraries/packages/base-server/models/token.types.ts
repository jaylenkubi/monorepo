import { UserEntityInterface } from "./user.types";

export interface TokenInterface {
  sub: UserEntityInterface;
  iat: number;
  exp: number;
}

export interface AccessTokenInterface {
  token: string;
  expires: Date;
}

export interface AuthTokenResponseInterface {
  emailVerificationToken: string;
  access: AccessTokenInterface;
  refresh: AccessTokenInterface;
}

export interface RegisterAndLoginResponseInterface {
  user?: UserEntityInterface;
  tokens?: AuthTokenResponseInterface;
}

export interface ValidateTokenResponseInterface {
  user?: UserEntityInterface;
  token?: TokenInterface;
}
