import { BaseDbType } from "./base.db.interface";

export enum UserType {
  ADMIN = 'ADMIN'
}
export enum TitleType {
  MR = 'Mr',
  MRS = 'Mrs',
  MISS = 'Miss',
}

export interface UserInterface {
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber?: string;
  title?: TitleType;
  password: string;
  type: UserType;
  emailVerified: boolean;
  defaultContextId: number;
}

export interface UserEntityInterface extends UserInterface, BaseDbType {}