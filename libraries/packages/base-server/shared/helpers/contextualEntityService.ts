import {BaseEntity, ObjectLiteral} from "typeorm";
import {Container} from "typedi";
import {logger} from "./logger";
import {RelationalAdapterInterface, REPO_CONTEXT_TOKEN, RepoRequestContext} from "../../adapters/relational.adapter";
import {GenericCrudService, GenericCrudServiceInterface, USER_CONTEXT_TOKEN} from "../../services/genericCrudService";


export const getContextualEntityService = <CI, EI extends ObjectLiteral>(
	entityName: string,
	entityClass: typeof BaseEntity | any,
	transactionId: string,
): GenericCrudServiceInterface<CI, EI> => {
	logger.info(`Creating service for entity ${entityName} with transactionId ${transactionId}`);
	return Container.of(transactionId)
		.set(USER_CONTEXT_TOKEN, {
			transactionId
		})
		.set(REPO_CONTEXT_TOKEN, {
			// ...Container.of(transactionId).get<RepoRequestContext>(REPO_CONTEXT_TOKEN),
			entityName,
			entity: entityClass,
			transactionId
		})
		.get<GenericCrudServiceInterface<CI, EI>>(GenericCrudService);
};