import "reflect-metadata";
import {AppDataSource} from "./data-source"
import {startServer} from "@monorepo/base-server/server";
import {ErrorMiddleware} from "@monorepo/base-server/middlewares/error.middleware";
import {UserController} from "./controller/UserController";
import {TransactionMiddleware} from "@monorepo/base-server/middlewares/transaction.middleware";

AppDataSource.initialize().then(async () => {
	await startServer({
		routePrefix: "/api",
		controllers: [UserController],
		middlewares: [TransactionMiddleware, ErrorMiddleware]
	}, 3000)
}).catch(error => console.log(error))
