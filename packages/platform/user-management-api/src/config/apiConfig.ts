import { baseInitializeConfig } from "@monorepo/base-server/config/config";
import packageJson from "../../package.json";

export const SERVICE_NAME = packageJson.name.split("/")[1];

export const initializeConfig = async () => {
  return baseInitializeConfig(jwtStrategy, __dirname);
};




