import { Request } from "express";
import { Strategy } from "passport-jwt";
import { TokenInterface } from "@monorepo/base-server/models/token.types";
import { logger } from "@monorepo/base-server/shared/helpers/logger";

export const extractTokenFromReq = (req: Request): string | undefined => {
  return req?.headers?.authorization?.replace('Bearer ', '');
};

const jwtVerify = async (req: any, jwtPayload: TokenInterface, next: Function) => {
  logger.info(`Checking token: ${JSON.stringify(jwtPayload)}`);
};


export const jwtStrategy = (secret: string): Strategy => {
  const jwtOptions = {
    jwtFromRequest: (req: Request): string | null => {
      const token = extractTokenFromReq(req);
      return token || null;
    },
    secretOrKey: secret,
    passReqToCallback: true,
  }
  return new Strategy(jwtOptions, (jwtPayload, done) => {
    done(null, jwtPayload);
  });
};
